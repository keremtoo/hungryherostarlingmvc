package {

    import com.kerem.utils.Constants;
    import com.kerem.views.GameRoot;

    import flash.desktop.NativeApplication;

    import flash.display.Bitmap;

    import flash.display.Sprite;
    import flash.events.Event;
    import flash.filesystem.File;
    import flash.geom.Rectangle;
    import flash.system.Capabilities;

    import starling.core.Starling;
    import starling.events.Event;
    import starling.textures.Texture;
    import starling.utils.AssetManager;
    import starling.utils.RectangleUtil;
    import starling.utils.ScaleMode;
    import starling.utils.formatString;

    [SWF(width='1024', height='768', backgroundColor='#000', frameRate='60')]
    public class Hungryherostarlingmvc extends Sprite {

        [Embed(source='/assets/fonts/Ubuntu-R.ttf', embedAsCFF='false', fontFamily='Ubuntu')]
        private static const UbuntuRegular : Class;
		// yeni aciklama
        [Embed(source='/assets/textures/1x/bgWelcome.jpg')]
        private static var Background : Class;

        public function Hungryherostarlingmvc()
        {
            var stageWidth : int = Constants.STAGE_WIDTH;
            var stageHeight : int = Constants.STAGE_HEIGHT;
            var iOS : Boolean = Capabilities.manufacturer.indexOf( "iOS" ) != -1;

            Starling.multitouchEnabled = true;
            Starling.handleLostContext = !iOS;

            var viewPort : Rectangle = RectangleUtil.fit(
                    new Rectangle( 0, 0, stageWidth, stageHeight ),
                    new Rectangle( 0, 0, Constants.STAGE_WIDTH, Constants.STAGE_HEIGHT ),
                    ScaleMode.SHOW_ALL
            );

            var scaleFactor : int = viewPort.width > 480 ? 1 : 2;
            var appDir : File = File.applicationDirectory;
            var assets : AssetManager = new AssetManager( scaleFactor );

            assets.verbose = Capabilities.isDebugger;
            assets.enqueue(
                    appDir.resolvePath( "assets/audio" ),
                    appDir.resolvePath( "assets/particles" ),
                    appDir.resolvePath( formatString( "assets/fonts/{0}x", scaleFactor ) ),
                    appDir.resolvePath( formatString( "assets/textures/{0}x", scaleFactor ) )
            );

            var background : Bitmap = new Background();
            background.x = viewPort.x;
            background.y = viewPort.y;
            background.width = viewPort.width;
            addChild( background );

            var mStarling : Starling = new Starling( GameRoot, stage, viewPort );
            mStarling.stage.stageWidth = stageWidth;
            mStarling.stage.stageHeight = stageHeight;
            mStarling.simulateMultitouch = false;
            mStarling.enableErrorChecking = Capabilities.isDebugger;

            mStarling.addEventListener(starling.events.Event.ROOT_CREATED,
                    function onRootCreated( event:Object, app:GameRoot ):void
                    {
                        mStarling.removeEventListener(starling.events.Event.ROOT_CREATED, onRootCreated);
                        removeChild( background );

                        var bgTexture : Texture = Texture.fromBitmap( background, false, false, scaleFactor );

                        app.start( bgTexture, assets );
                        mStarling.start();
                    }
            );

            NativeApplication.nativeApplication.addEventListener(
                    flash.events.Event.ACTIVATE, function( e:* ):void { mStarling.start(); }
            );

            NativeApplication.nativeApplication.addEventListener(
                    flash.events.Event.DEACTIVATE, function( e:* ):void { mStarling.stop(); }
            );
        }
    }
}
