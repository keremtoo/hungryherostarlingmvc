/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 19.11.2013
 * Time: 12:00
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.events {
    import starling.events.Event;

    public class GameSceneEvent extends Event {

        public static const ADDVIEW    : String = "add_view";
        public static const CHANGEVIEW : String = "change_view";

        private var _result 			: Boolean;

        public function GameSceneEvent( type:String, result:Boolean, bubbles:Boolean = false )
        {
            super( type, bubbles );
            _result = result;
        }

        public function get result():Boolean
        {
            return _result;
        }
    }
}
