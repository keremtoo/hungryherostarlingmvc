/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 19.11.2013
 * Time: 10:37
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.providers {
    import com.creativebottle.starlingmvc.beans.BeanProvider;
    import com.kerem.controllers.GameController;
    import com.kerem.mediators.AboutSceneMediator;
    import com.kerem.mediators.GameMediator;
    import com.kerem.mediators.SplashSceneMediator;
    import com.kerem.mediators.WelcomeSceneMediator;

    public class GameObjectProvider extends BeanProvider {
        public function GameObjectProvider()
        {
            beans = [
                    new GameController( ),
                    new GameMediator( ),
                    new SplashSceneMediator( ),
                    new WelcomeSceneMediator( ),
                    new AboutSceneMediator( )
            ];
        }
    }
}
