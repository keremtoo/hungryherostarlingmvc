/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 18.11.2013
 * Time: 16:02
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.utils {
    import starling.errors.AbstractClassError;

    public class Constants {

        public static const STAGE_WIDTH     : int = 1024;
        public static const STAGE_HEIGHT    : int = 768;

        public function Constants()
        {
            throw new AbstractClassError( "Constants instance invalid!" );
        }
    }
}
