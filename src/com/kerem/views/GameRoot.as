/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 18.11.2013
 * Time: 17:07
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.views {

    import com.creativebottle.starlingmvc.StarlingMVC;
    import com.creativebottle.starlingmvc.config.StarlingMVCConfig;
    import com.creativebottle.starlingmvc.views.ViewManager;
    import com.kerem.providers.GameObjectProvider;

    import starling.display.Image;

    import starling.display.Sprite;
    import starling.textures.Texture;
    import starling.utils.AssetManager;

    public class GameRoot extends Sprite {
        
        private static var sAssets : AssetManager;
        
        public function GameRoot()
        {
            super();
        }

        public function start( background:Texture, assets:AssetManager ):void
        {
            sAssets = assets;

            addChild( new Image( background ) );

            var config : StarlingMVCConfig = new StarlingMVCConfig();
            config.eventPackages = ["com.kerem.events"];
            config.viewPackages  = ["com.kerem.views"];

            var beans : Array = [new GameObjectProvider(), new ViewManager( this )];

            var starlingMVC : StarlingMVC = new StarlingMVC( this, config, beans );
            starlingMVC.initialized = true;
        }

        public static function get assets():AssetManager
        {
            return sAssets;
        }
    }
}
