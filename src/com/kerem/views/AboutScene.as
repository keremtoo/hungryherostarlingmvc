/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 21.11.2013
 * Time: 11:23
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.views {
    import com.kerem.utils.Constants;

    import flash.display.BitmapData;

    import flash.display.Shape;

    import starling.core.Starling;

    import starling.display.Button;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.textures.Texture;

    public class AboutScene extends Sprite {

        public var backButton           : Button;

        [PostConstruct]
        public function postConstruct():void
        {
            var scale:Number = Starling.contentScaleFactor;

            var rounded : Shape = new Shape( );
            rounded.graphics.beginFill( 0x0, .6 );
            rounded.graphics.drawRoundRect( 0, 0, 1004, 748, 20, 20 );
            rounded.graphics.endFill( );

            var bmpData : BitmapData = new BitmapData( rounded.width, rounded.height, true, 0x0 );
            bmpData.draw( rounded );
            var bgTexture : Texture = Texture.fromBitmapData( bmpData, false, false, scale );
            var bgImage : Image = new Image( bgTexture );
            bgImage.x = bgImage.y = 10;
            addChild( bgImage );

            backButton = new Button( GameRoot.assets.getTexture( "about_backButton" ) );
            backButton.x = ( Constants.STAGE_WIDTH - backButton.width ) / 2;
            backButton.y = ( Constants.STAGE_HEIGHT - backButton.width ) + 20;
            addChild( backButton );
        }

        [PreDestroy]
        public function preDestroy():void
        {
        }
    }
}
