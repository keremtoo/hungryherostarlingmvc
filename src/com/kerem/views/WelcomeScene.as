/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 19.11.2013
 * Time: 18:38
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.views {
    import starling.animation.Transitions;
    import starling.core.Starling;
    import starling.display.Button;
    import starling.display.Image;
    import starling.display.Sprite;
    import starling.events.Event;

    public class WelcomeScene extends Sprite {

        public var playButton               : Button;
        public var aboutButton              : Button;

        private var title                   : Image;
        private var hero                    : Image;


        [PostConstruct]
        public function postConstruct():void
        {
            this.touchable = false;

            title = new Image( GameRoot.assets.getTexture( "welcome_title" ) );
            title.x = 580;
            title.y = 65;
            addChild( title );

            hero = new Image( GameRoot.assets.getTexture( "welcome_hero" ) );
            hero.x = -hero.width;
            hero.y = 100;
            addChild( hero );

            playButton = new Button( GameRoot.assets.getTexture( "welcome_playButton" ) );
            playButton.x = 580;
            playButton.y = 340;
            addChild( playButton );

            aboutButton = new Button( GameRoot.assets.getTexture( "welcome_aboutButton" ) );
            aboutButton.x = 420;
            aboutButton.y = 460;
            addChild( aboutButton );

            Starling.juggler.tween(
                    hero, 2, { x:110, rotation:Transitions.EASE_IN_OUT, onComplete:onTriggered }
            );

            addEventListener(Event.ENTER_FRAME, heroAnimation);
        }

        private function onTriggered():void
        {
            this.touchable = true;
        }

        private function heroAnimation( event:Event ):void
        {
            var currentDate : Date = new Date( );
            hero.y = 100 + ( Math.cos( currentDate.getTime() * 0.002 ) * 25 );
            playButton.y = 340 + ( Math.cos( currentDate.getTime() * 0.002 ) * 10 );
            aboutButton.y = 460 + ( Math.cos( currentDate.getTime() * 0.002 ) * 10 );
            title.y = 62 + ( Math.cos( currentDate.getTime() * 0.002 ) * 10 );
        }

        [PreDestroy]
        public function preDestroy():void
        {
            title.dispose( );
            hero.dispose( );
            aboutButton.dispose( );
            playButton.dispose( );
        }
    }
}
