/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 19.11.2013
 * Time: 10:53
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.views {
    import com.kerem.events.GameSceneEvent;
    import com.kerem.utils.Constants;
    import com.kerem.utils.ProgressBar;

    import starling.core.Starling;

    import starling.display.Sprite;
    import starling.events.EventDispatcher;

    public class SplashScene extends Sprite {

        [Dispatcher] public var dispatcher : EventDispatcher;

        [PostConstruct]
        public function postConstruct():void
        {
            var progressBar : ProgressBar = new ProgressBar( 175, 25 );
            progressBar.x = ( Constants.STAGE_WIDTH - progressBar.width ) / 2;
            progressBar.y = ( Constants.STAGE_HEIGHT - progressBar.height ) / 2;
            addChild( progressBar );

            GameRoot.assets.loadQueue( function onProgress( ratio:Number ):void
                    {
                        progressBar.ratio = ratio;

                        if ( ratio == 1 )
                            Starling.juggler.delayCall( function():void
                                    {
                                        progressBar.removeFromParent( true );
                                        dispatcher.dispatchEventWith( GameSceneEvent.CHANGEVIEW, false, WelcomeScene );
                                    }, 0.15
                            );
                    }
            );
        }
    }
}
