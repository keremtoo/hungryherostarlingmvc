/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 19.11.2013
 * Time: 10:46
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.mediators {
    import com.kerem.views.SplashScene;

    public class SplashSceneMediator {

        private var view : SplashScene;


        [ViewAdded]
        public function viewAdded( view:SplashScene ):void
        {
            this.view = view;
            trace( ">> SplashSceneMediator : SplashScene Added to Stage" );
        }

        [ViewRemoved]
        public function viewRemoved( view:SplashScene ):void
        {
            this.view = null;
            trace( ">> SplashSceneMediator : SplashScene Removed to Stage" );
        }
    }
}
