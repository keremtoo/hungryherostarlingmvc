/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 22.11.2013
 * Time: 14:02
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.mediators {
    import com.creativebottle.starlingmvc.events.EventMap;
    import com.kerem.controllers.GameController;
    import com.kerem.views.AboutScene;
    import com.kerem.views.WelcomeScene;

    import starling.events.Event;

    import starling.events.EventDispatcher;

    public class AboutSceneMediator {

        [Dispatcher] public var dispatcher  : EventDispatcher;

        [Inject] public var gameController  : GameController;

        private var view                    : AboutScene;
        private var backView                : Class;
        private var eventMap                : EventMap = new EventMap( );

        [ViewAdded]
        public function viewAdded( view:AboutScene ):void
        {
            this.view = view;
            backView = WelcomeScene;

            eventMap.addMap( view.backButton, Event.TRIGGERED, backButtonTriggered );
        }

        [ViewRemoved]
        public function viewRemoved( view:AboutScene ):void
        {
            backView = null;
            eventMap.removeAllMappedEvents();

            this.view = null;
        }

        private function backButtonTriggered( event:Event ):void
        {
            gameController.changeView( backView );
        }
    }
}
