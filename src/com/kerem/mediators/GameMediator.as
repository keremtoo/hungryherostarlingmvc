/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 19.11.2013
 * Time: 10:39
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.mediators {
    import com.kerem.views.GameRoot;

    public class GameMediator {

        private var view : GameRoot;

        [ViewAdded]
        public function viewAdded( view:GameRoot ):void
        {
            this.view = view;
            trace( ">> GameMediator : GameRoot Added to Stage" );
        }

        [ViewRemoved]
        public function viewRemoved( view:GameRoot ):void
        {
            this.view = null;
            trace( ">> GameMediator : GameRoot Removed to Stage" );
        }
    }
}
