/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 20.11.2013
 * Time: 18:13
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.mediators {
    import com.creativebottle.starlingmvc.events.EventMap;
    import com.kerem.controllers.GameController;
    import com.kerem.events.GameSceneEvent;

    import starling.events.EventDispatcher;

    import com.kerem.views.AboutScene;
    import com.kerem.views.GameInScene;
    import com.kerem.views.WelcomeScene;

    import starling.events.Event;

    public class WelcomeSceneMediator {

        [Dispatcher] public var dispatcher  : EventDispatcher;
        [Inject] public var gameController  : GameController;

        private var view                    : WelcomeScene;
        private var nextView                : Class;
        private var addScene                 : AboutScene;
        private var eventMap                : EventMap = new EventMap();

        [ViewAdded]
        public function viewAdded( view:WelcomeScene ):void
        {
            this.view = view;
            nextView = GameInScene;
            addScene = new AboutScene( );

            eventMap.addMap( view.playButton, Event.TRIGGERED, playButtonTriggered );
            eventMap.addMap( view.aboutButton, Event.TRIGGERED, aboutButtonTriggered );
        }


        [ViewRemoved]
        public function viewRemoved( view:WelcomeScene ):void
        {
            nextView = null;
            addScene.dispose();
            eventMap.removeAllMappedEvents();

            this.view = null;
        }

        private function playButtonTriggered( event:Event ):void
        {
            gameController.changeView( nextView );
        }

        private function aboutButtonTriggered( event:Event ):void
        {
            dispatcher.dispatchEventWith( GameSceneEvent.ADDVIEW, false, addScene );
        }
    }
}
