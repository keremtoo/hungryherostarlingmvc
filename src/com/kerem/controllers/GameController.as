/**
 * Created with IntelliJ IDEA.
 * User: Kerem.Ozdemir
 * Date: 19.11.2013
 * Time: 14:20
 * To change this template use File | Settings | File Templates.
 */
package com.kerem.controllers {
    import com.creativebottle.starlingmvc.views.ViewManager;
    import com.kerem.views.SplashScene;

    import starling.display.Sprite;

    import starling.events.EventDispatcher;

    public class GameController {

        [Dispatcher] public var dispatcher  : EventDispatcher;

        [Inject] public var viewManager     : ViewManager;


        [PostConstruct]
        public function postConstruct():void
        {
            trace( ">> set view SplashScene" );
            viewManager.setView( SplashScene );
        }

        [EventHandler(event="GameSceneEvent.CHANGEVIEW", properties="data")]
        public function changeView( data:Class ):void
        {
            trace( ">> set view Data" );
            viewManager.setView( data );
        }

        [EventHandler(event="GameSceneEvent.ADDVIEW", properties="data")]
        public function addView( data:Sprite ):void
        {
            trace( ">> add view Data" );
            viewManager.addView( data );
        }
    }
}
